package SeleniumCode;


import PO.PaginaInicioPO;
import PO.adminiPo;
import PO.datosPersonalesPO;
import Utiles.DatosSistemaTest;

import cucumber.api.java.bs.Dato;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.log4j.PropertyConfigurator;
import org.junit.Assert;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.io.IOException;

public class Test_Ripley {

    /** ***************************Llamado Property proyecto ****************************************** **/
    private final String url = DatosSistemaTest.getDatoProperties("url");
    private final String us  = DatosSistemaTest.getDatoProperties("usuario");
    private final String pas = DatosSistemaTest.getDatoProperties("pass");
    private final String nom = DatosSistemaTest.getDatoProperties("Nombre");
    private final String sno = DatosSistemaTest.getDatoProperties("SegundoNombre");
    private final String ape = DatosSistemaTest.getDatoProperties("Apellido");
    private final String edad = DatosSistemaTest.getDatoProperties("edad");
    private final String nacim = DatosSistemaTest.getDatoProperties("fecNaci");
    private final String fechaEx = DatosSistemaTest.getDatoProperties("fechaEx");
    private final String numer = DatosSistemaTest.getDatoProperties("numeroLi");
    private final String hobies = DatosSistemaTest.getDatoProperties("hobie");
    private final String nick = DatosSistemaTest.getDatoProperties("nick");
    private final String estadomil = DatosSistemaTest.getDatoProperties("estadomil");

    /** ***************************Llamado Log4j y Driver funcionamiento Script ****************************************** **/
    String log4jConfPath = "./src/test/resources/log4j.properties";
    WebDriver driver;
    /** ***************************Llamado a los Page Opbejct****************************************** **/

    /** LLamado a las clases **/
    PaginaInicioPO PI;
    adminiPo AP;
    datosPersonalesPO DP;
    @Given("^que Juan necesita crear un empleado en el OrageHR$")
    public void que_Juan_necesita_crear_un_empleado_en_el_OrageHR() {
        PropertyConfigurator.configure(log4jConfPath);
        WebDriverManager.chromedriver().setup(); //bonigarcia
        ChromeOptions options = new ChromeOptions();
        options.addArguments("enable-automation");
        //options.addArguments("--headless","--window-size=2000,2080","--ignore-certificate-errors");
        options.addArguments("--start-maximized","--ignore-certificate-errors");
        options.addArguments("--no-sandbox");
        options.setPageLoadStrategy(PageLoadStrategy.NONE);
        driver = new ChromeDriver(options);
        this.PI = new PaginaInicioPO(driver);
        this.AP = new adminiPo(driver);
        this.DP = new datosPersonalesPO(driver);
        PI.ingresoAplicacion(url,us,pas);

    }

    @When("^el realiza el ingreso del registro en la aplicación$")
    public void el_realiza_el_ingreso_del_registro_en_la_aplicación() throws IOException {
        AP.datoPersonales(nom,sno,ape);
        DP.datosPersonales(edad,nacim,numer,fechaEx,hobies,nick,estadomil);
        DP.guardarDatos();
    }

    @Then("^el visualiza el nuevo empleado en el aplicativo$")
    public void el_visualiza_el_nuevo_empleado_en_el_aplicativo() throws Exception {
        //AP.listarEmple(nom,sno,ape);
        DP.imagenFinal();

    }



}
