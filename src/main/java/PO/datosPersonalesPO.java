package PO;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.io.IOException;

public class datosPersonalesPO extends BasePage{
    public datosPersonalesPO (WebDriver driver){
        super(driver);
    }
    @FindBy(id ="otherId") WebElement Edad;
    @FindBy(id="emp_birthday") WebElement fechaNacimieto;
    /** Estado **/
    @FindBy(xpath = "(//input[contains(@class,'select-dropdown')])[1]") WebElement estado;
    @FindBy (xpath = "//span[contains(text(),'Married')]") WebElement sEstado;
    /** Genero **/
    @FindBy(xpath = "(//input[contains(@class,'select-dropdown')])[2]") WebElement genero;
    @FindBy(xpath = "//span[contains(text(),'Male')]") WebElement sGenero;
    /** Nacionalidad **/
    @FindBy(xpath = "(//input[contains(@class,'select-dropdown')])[3]") WebElement nacionalidad;
    @FindBy(xpath = "//li//span[contains(text(),'Chilean')]") WebElement sNacionalidad;

    @FindBy(id = "licenseNo") WebElement numeroLicencia;
    @FindBy (id = "emp_dri_lice_exp_date") WebElement fechaExLice;

    @FindBy(id="nickName") WebElement nick;
    @FindBy(id="militaryService") WebElement estadoMili;
    @FindBy(xpath = "(//input[contains(@class,'select-dropdown')])[4]") WebElement tipoSangre;
    @FindBy(xpath = "//li//span[contains(text(),'AB')]") WebElement sTipoSangre;
    @FindBy(id = "5") WebElement Hobies;
    @FindBy(xpath = "//button[contains(@ng-click,'vm.onNextStep()')]") WebElement btnNex;
    /**guardar empleado **/
    @FindBy(xpath = "(//input[contains(@class,'select-dropdown')])[6]") WebElement region;
    @FindBy(xpath = "//li/span[contains(text(),'Region-1')]") WebElement sRegion;
    @FindBy(xpath = "(//input[contains(@class,'select-dropdown')])[7]") WebElement fte;
    @FindBy(xpath = "//li/span[contains(text(),'0.75')]") WebElement sFte;
    @FindBy(xpath = "(//input[contains(@class,'select-dropdown')])[8]") WebElement deptemp;
    @FindBy(xpath = "//li/span[contains(text(),'Sub unit-1')]") WebElement sDeptemp;

    @FindBy(xpath = "//button[contains(@ng-click,'vm.onFinish()')]") WebElement btnGuardar;

    @FindBy(id = "small-title") WebElement nombreEmple;


    public void datosPersonales(String edad,String nacimiento, String lic, String fechaEx, String hob, String nikN, String estadoMil){
        waitFor(2);
        Edad.click();
        Edad.sendKeys(edad);
        fechaNacimieto.sendKeys(nacimiento);
        Log("Se ingresa Estado ");
        position(driver,estado);
        position(driver,sEstado);
        Log("Se ingresa genero ");
        position(driver,genero);
        position(driver,sGenero);
        Log("Se ingresa Nacionalidad ");
        position(driver,nacionalidad);
        position(driver,sNacionalidad);
        numeroLicencia.click();
        numeroLicencia.sendKeys(lic);
        fechaExLice.sendKeys(fechaEx);
        nick.sendKeys(nikN);
        estadoMili.sendKeys(estadoMil);

        position(driver,tipoSangre);
        position(driver,sTipoSangre);

        Hobies.sendKeys(hob);
        btnNex.click();



    }

    public void guardarDatos() throws IOException {
        waitFor(2);
        scroll();
        Log("Se selcciona region");
        position(driver,region);
        position(driver,sRegion);
        Log("Se selcciona FTE");
        position(driver,fte);
        position(driver,sFte);
        Log("Se selcciona Dep");
        position(driver,deptemp);
        position(driver,sDeptemp);
        Log("Se guarda informacion ");
        btnGuardar.click();

    }

    public void imagenFinal() throws Exception {
        waitFor(8);
        if(isVisible(nombreEmple)){
            String nom = nombreEmple.getText();
            Log("Se visualiza el nombre del empleado : "+ nom);
            waitFor(5);
            CapturaImagen(driver,"evidencia01");
            driver.close();
        }else{
            Log("Algo ocurrio mal ");
        }


    }
}
