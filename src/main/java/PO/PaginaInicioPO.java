package PO;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class PaginaInicioPO extends BasePage{

    public PaginaInicioPO (WebDriver driver){
        super(driver);
    }
    @FindBy(id = "txtUsername")
    WebElement usuario;
    @FindBy(id = "txtPassword")
    WebElement contraseña;
    @FindBy(xpath = "//button[contains(@class,'btn btn-primary dropdown-toggle')]")
    WebElement selcRol;
    @FindBy(xpath = "(//li//a[contains(text(),'Administrator')])[2]")
    WebElement seleccionRoll;

    public void ingresoAplicacion(String url, String usu, String pass){
        driver.get(url);
        waitFor(1);
        Log("Se ingresa a la URL : " + url);
        usuario.sendKeys(usu);
        contraseña.sendKeys(pass);
        selcRol.click();
        seleccionRoll.click();
        Log("Se ingresa en pagina ");
        waitFor(2);
    }

}
