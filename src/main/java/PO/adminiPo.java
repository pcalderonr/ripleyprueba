package PO;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class adminiPo extends BasePage{
    public adminiPo (WebDriver driver){
        super(driver);
    }
    @FindBy(xpath = "//span[contains(text(),'PIM')]")
    WebElement btnPim;
    @FindBy(xpath = "(//span[contains(text(),'Add Employee')])[2]")
    WebElement btnAdd;

    @FindBy(id = "firstName")
    WebElement inpNombre;
    @FindBy(id = "middleName")
    WebElement inpMidName;
    @FindBy(id="lastName")
    WebElement inpLasName;

    @FindBy(xpath = "(//input[contains(@class,'select-dropdown')])[2]")
    WebElement dLocation;
    @FindBy(xpath = "//li//span[contains(text(),'Australian Regional HQ')]")
    WebElement sLocation;
    @FindBy(id="systemUserSaveBtn") WebElement btnNex;

    @FindBy(xpath = "//span[contains(text(),'Employee List')]") WebElement btnEmplet;
    @FindBy(id = "employee_name_quick_filter_employee_list_value") WebElement buscador;
    @FindBy(id="quick_search_icon") WebElement lupa;

    @FindBy(xpath = "(//td[contains(text(),'Patricio Ignacio Calderon ')])[1]") WebElement sEmple;


    public void datoPersonales(String nombre, String sNombre, String Apellido){
        waitFor(3);
        if(isVisible(btnPim)) {
            Log("es visible le btn ");
            waitFor(1);
            btnPim.click();
            Log("Seleccionamos boton 'PIM' ");
        }else{
            Log("No se cargo el btn PIM");
        }


        Log("Seleccionamos boton 'Add Employe' ");
        waitFor(1);
        btnAdd.click();
        waitFor(5);
        Log("Se imgresa el primer nombre ");
        inpNombre.click();
        inpNombre.sendKeys(nombre);
        Log("Se imgresa el segundo nombre ");
        inpMidName.click();
        inpMidName.sendKeys(sNombre);
        Log("Se imgresa el apelldio ");
        inpLasName.click();
        inpLasName.sendKeys(Apellido);
        Log("Seleccionamos la locacion ");
        //dLocation.click();
        position(driver,dLocation);

        Log("Seleccionamos locacion real ");
        position(driver,sLocation);
        btnNex.click();
        waitFor(2);


    }
   /* public void listarEmple(String nombre,String segundoNombre, String Apellido){

        waitFor(5);
        if (isVisible(btnEmplet)){
            Log("Esvisible");
            waitFor(5);
            Log("Se presiona para listar empleado ");
            btnEmplet.click();
            waitFor(2);

        }


        buscador.click();
        buscador.sendKeys(nombre + " " + segundoNombre + " " + Apellido);
        lupa.click();
        waitFor(2);
        Log("SAe selecciona el primer empleado en aparecer ");
        sEmple.click();
        waitFor(2);


    }*/
}
